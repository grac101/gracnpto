﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kartoteka.Osoba
{
    class Osoba
    {
        private string imie;
        private string nazwisko;

        public Osoba(string imie, string nazwisko)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
        }

        public string getImie()
        {
            return imie;
        }

        public string getNazwisko()
        {
            return nazwisko;
        }
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Osoba))
            {
                return false;
            }

            Osoba other = (Osoba)obj;
            return imie.Equals(other.getImie()) && nazwisko.Equals(other.getNazwisko());
        }
    }
}
