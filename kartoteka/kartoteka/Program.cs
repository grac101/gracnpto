﻿using impl;
using mockup;
using kartoteka.Osoba;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static mkartoteka kartoteka = new mkartoteka(); //zmiana    static ikartoteka kartoteka = new ikartoteka(); na         static mkartoteka kartoteka = new mkartoteka();

        static void Main(string[] args)
        {
            bool exit = false;

            while (!exit)
            {
                Console.WriteLine("Wybierz opcję:");
                Console.WriteLine("1. Dodaj osobę do kartoteki");
                Console.WriteLine("2. Usuń osobę z kartoteki");
                Console.WriteLine("3. Wyświetl rozmiar kartoteki");
                Console.WriteLine("4. Sprawdź, czy osoba jest w kartotece");
                Console.WriteLine("5. Pobierz osobę z kartoteki");
                Console.WriteLine("0. Wyjdź");

                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        dodajOsobe();
                        break;
                    case "2":
                        usunOsobe();
                        break;
                    case "3":
                        wyswietlRozmiar();
                        break;
                    case "4":
                        sprawdzCzyZawiera();
                        break;
                    case "5":
                        pobierzOsobe();
                        break;
                    case "0":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Nieznana opcja");
                        break;
                }

                Console.WriteLine();
            }
        }

        static void dodajOsobe()
        {
            Console.WriteLine("Podaj imię:");
            string imie = Console.ReadLine();

            Console.WriteLine("Podaj nazwisko:");
            string nazwisko = Console.ReadLine();

            Osoba o = new Osoba(imie, nazwisko);

            kartoteka.dodaj(o);
        }

        static void usunOsobe()
        {
            Console.WriteLine("Podaj imię:");
            string imie = Console.ReadLine();

            Console.WriteLine("Podaj nazwisko:");
            string nazwisko = Console.ReadLine();

            Osoba o = new Osoba(imie, nazwisko);

            kartoteka.usun(o);
        }

        static void wyswietlRozmiar()
        {
            Console.WriteLine("Rozmiar kartoteki: " + kartoteka.rozmiar());
        }

        static void sprawdzCzyZawiera()
        {
            Console.WriteLine("Podaj imię:");
            string imie = Console.ReadLine();

            Console.WriteLine("Podaj nazwisko:");
            string nazwisko = Console.ReadLine();

            Osoba o = new Osoba(imie, nazwisko);

            bool zawiera = kartoteka.CzyZawiera(o);

            if (zawiera)
            {
                Console.WriteLine("Osoba znajduje się w kartotece");
            }
            else
            {
                Console.WriteLine("Osoba nie znajduje się w kartotece");
            }
        }

        static void pobierzOsobe()
        {
            Console.WriteLine("Podaj indeks osoby:");
            int index = int.Parse(Console.ReadLine());

            Osoba o = kartoteka.pobierz(index);

            Console.WriteLine("Imię: " + o.getImie());
            Console.WriteLine("Nazwisko: " + o.getNazwisko());
        }
    }
}