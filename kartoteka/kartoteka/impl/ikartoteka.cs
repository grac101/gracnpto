﻿using kartoteka.Osoba;
using System;
using System.Collections.Generic;

namespace impl
{
    class ikartoteka
    {
        private List<Osoba> kartoteka;

        public ikartoteka()
        {
            kartoteka = new List<Osoba>();
        }

        public void dodaj(Osoba o)
        {
            kartoteka.Add(o);
        }

        public void usun(Osoba o)
        {
            kartoteka.Remove(o);
        }

        public int rozmiar()
        {
            return kartoteka.Count;
        }

        public bool CzyZawiera(Osoba o)
        {
            foreach (Osoba os in kartoteka)
            {
                if (os.Equals(o))
                {
                    return true;
                }
            }
            return false;
        }

        public Osoba pobierz(int index)
        {
            return kartoteka[index];
        }
    }
}





